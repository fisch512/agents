/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agent_threads;

import java.util.Scanner;

/**
 *
 * @author Matt
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Environment env = new Environment(2, 2, 1, 6);
        Scanner in = new Scanner(System.in);
        int command = -1;

        while (command != 0) {
            System.out.println();
            System.out.println("Enter command:");
            System.out.println("0) Exit");
            System.out.println("1) Print Agents");
            System.out.println("2) Print Particles");
            System.out.println("3) Pause Agent threads");
            System.out.println("4) Resume Agent threads");
            System.out.println("5) Terminate all Agent threads");
            System.out.println("6) Count Particles");
            System.out.println("7) Account for Particles");
            System.out.print("Enter command: ");

            command = in.nextInt();

            switch (command) {
                case 0:
                    env.cancel();
                    break;
                case 1:
                    env.printAgents();
                    break;
                case 2:
                    env.printParticles();
                    break;
                case 3:
                    env.pause();
                    break;
                case 4:
                    env.resume();
                    break;
                case 5:
                    env.cancel();
                    break;
                case 6:
                    env.countParticles();
                    break;
                case 7:
                    env.particleAccounting();
                    break;
                default:
                    System.out.println("Invalid command");
                    break;
            }
        }

        System.out.println("Program Terminated");
    }
}
