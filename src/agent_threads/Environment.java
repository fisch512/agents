/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agent_threads;

import java.util.LinkedList;
import java.util.Random;


/**
 *
 * @author Matt
 */
public class Environment {

    private int m_rows, m_cols, m_numBots, m_numParticles;
    private boolean m_paused, m_cancelled;
    private EnvCell[][] m_envGrid;
    private Agent[] m_botList;
    private LinkedList<Integer> m_numbers;

    public Environment(int rows, int cols, int numBots, int numParticles) {
        m_rows = rows;
        m_cols = cols;
        m_numBots = numBots;
        m_numParticles = numParticles;
        m_paused = false;
        m_cancelled = false;
        m_envGrid = new EnvCell[rows][cols];
        m_botList = new Agent[numBots];
        m_numbers = new LinkedList<Integer>();
        init();
    }

    public boolean isPaused() {
        return m_paused;
    }

    public void start() {
        for (int i = 0; i < m_botList.length; i++) {
            m_botList[i].start();
        }
    }

    public void pause() {
        m_paused = true;
    }

    public void cancel() {
        m_cancelled = true;
        resume();
    }

    public void countParticles() {
        int sum = 0;
        for (int i = 0; i < m_rows; i++) {
            for (int j = 0; j < m_cols; j++) {
                sum += m_envGrid[i][j].getNumParticles();
            }
        }
        System.out.println("Total particles across cells: " + sum);
    }

    public void particleAccounting() {
        boolean goodRef = true;
        LinkedList<Integer> tempNumbers = new LinkedList<Integer>(m_numbers);

        pause();
//        try {
//            Thread.sleep(1000);
//        }
//        catch (InterruptedException ex) {
//            System.out.println("Interrupt - " + ex.toString());
//        }

        LinkedList<Particle> particles = new LinkedList();
        for (int i = 0; i < m_rows; i++) {
            for (int j = 0; j < m_cols; j++) {
                particles.addAll(m_envGrid[i][j].getParticles());
            }
        }

        for (int i = 0; i < particles.size(); i++) {
            if (tempNumbers.contains(particles.get(i).getId())) {
                tempNumbers.remove((Integer)particles.get(i).getId());
            }
            else {
                System.out.println("Accounting: FAILURE");
                System.out.println(particles.get(i).getId());
                System.out.println(tempNumbers);
                goodRef = false;
                break;
            }
        }

        if (goodRef) {
            System.out.println("Accounting: SUCCESS: Each particle referenced once");
        }

        resume();

    }

    public void resume() {
        if (m_paused == true && m_cancelled == false) {
            m_paused = false;
            for (int i = 0; i < m_botList.length; i++) {
                System.out.println("...Agent " + m_botList[i].getIdNum() + " has resumed");
                m_botList[i].resumeAgent();
            }
        }
    }

    public EnvCell getEnvCell(int row, int column) {
        return m_envGrid[row][column];
    }

    public int getRows() {
        return m_rows;
    }

    public int getCols() {
        return m_cols;
    }

    private void init() {
        Random rng = new Random();

        System.out.println("Total number of particles: " + m_numParticles);
        System.out.println("Total number of bots: " + m_numBots);

        //Create all of the Enviroment Cells
        for (int i = 0; i < m_rows; i++) {
            for (int j = 0; j < m_cols; j++) {
                m_envGrid[i][j] = new EnvCell();
            }
        }

        //Randomly distribute the particles
        for (int i = 0; i < m_numParticles; i++) {
            Particle p = new Particle(i + 1);
            int row = rng.nextInt(m_rows);
            int col = rng.nextInt(m_cols);
            m_numbers.add(i + 1);

            m_envGrid[row][col].addParticle(p);

        }

        //Randomly distribute the bots
        for (int i = 0; i < m_numBots; i++) {
            int row = rng.nextInt(m_rows);
            int col = rng.nextInt(m_cols);
            Agent a = new Agent(this, i + 1, row, col);
            
            m_botList[i] = a;
            System.out.println("Starting bot: " + (i+1));
            a.start();
        }
    }

    public boolean isCancelled() {
        return m_cancelled;
    }

    public void printAgents() {
        for (int i = 0; i < m_rows; i++) {
            for (int j = 0; j < m_cols; j++) {
                System.out.print(m_envGrid[i][j].getNumAgents() + " ");
            }
            System.out.println();
        }
    }

    public void printParticles() {
        for (int i = 0; i < m_rows; i++) {
            for (int j = 0; j < m_cols; j++) {
                System.out.print(m_envGrid[i][j].getNumParticles() + " ");
            }
            System.out.println();
        }
    }
}
