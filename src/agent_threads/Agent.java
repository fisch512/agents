/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agent_threads;

import java.util.Random;

/**
 *
 * @author Matt
 */
public class Agent extends Thread {

    private static final double PER_PICKUP = 0.3;
    private static final double PER_DROP = 0.3;
    private Particle m_particle;
    private Environment m_env;
    private int m_row, m_col, m_maxRows, m_maxCols, m_id;
    private Random m_rng;

    public Agent(Environment env, int id, int row, int col) {
        //super();
        m_env = env;
        m_id = id;
        m_row = row;
        m_col = col;

        m_env.getEnvCell(row, col).addAgent();

        m_maxRows = env.getRows();
        m_maxCols = env.getCols();

        //For more "trully" random, random numbers
        Random temp_rng = new Random();
        m_rng = new Random(temp_rng.nextLong());
    }

    @Override
    public void run() {
        while (!m_env.isCancelled()) {
            //System.out.println("Got here: 1");
            if (m_env.isPaused()) {
                //System.out.println("Got here: 2");
                if (m_particle != null) {
                    dropNow();
                }

                System.out.println("Agent " + m_id + " is pausing...");

                try {
                    synchronized (this) {
                        wait();
                    }
                }
                catch (InterruptedException e) {
                    System.out.println("Interrupted - " + e.toString());
                }
            }
            move();

            if (m_particle != null) {
                drop();
            }
            else {
                pickUp();
            }
        }

        if (m_particle != null) {
            dropNow();
        }

        System.out.println("Agent: " + m_id + " done");

    }

    public void resumeAgent() {
        synchronized (this) {
            notify();
        }
    }

    public int getIdNum() {
        return m_id;
    }

    //Randomly moves the bot either vertically, horizontally, or diagaonlly
    private void move() {
        //System.out.println("Got here: Move");
        m_env.getEnvCell(m_row, m_col).removeAgent();

        if (m_rng.nextDouble() <= (1.0 / 3.0)) {
            moveHor();
        }
        else if (m_rng.nextDouble() <= (2.0 / 3.0)) {
            moveVer();
        }
        else {
            moveHor();
            moveVer();
        }

        m_env.getEnvCell(m_row, m_col).addAgent();
    }

    //Randomly moves the agent up or down
    //Takes the grid walls into consideration
    private void moveVer() {
        if (m_row != 0) {
            if (m_row != (m_maxRows - 1)) {
                if (m_rng.nextDouble() <= 0.5) {
                    m_row++;                //move down the grid
                }
                else {
                    m_row--;                //move up the grid
                }
            }
            else {
                m_row--;                    //move up the grid
            }
        }
        else {
            m_row++;                        //move down the grid
        }
    }

    //Randomly moves the agent left or right
    //Takes the grid walls into consideration
    private void moveHor() {
        if (m_col != 0) {
            if (m_col != (m_maxCols - 1)) {
                if (m_rng.nextDouble() <= 0.5) {
                    m_col++;                //moves right on the grid
                }
                else {
                    m_col--;                //moves left on the grid
                }
            }
            else {
                m_col--;                    //moves left on the grid
            }
        }
        else {
            m_col++;                        //moves right on the grid
        }
    }

    private void pickUp() {
        if (flipCoin(PER_PICKUP)) {
            m_particle = m_env.getEnvCell(m_row, m_col).removeParticle();
        }
    }

    private void drop() {
        if (flipCoin(PER_DROP)) {
            m_env.getEnvCell(m_row, m_col).addParticle(m_particle);
            m_particle = null;
        }
    }

    private void dropNow() {
            m_env.getEnvCell(m_row, m_col).addParticle(m_particle);
            m_particle = null;
    }

    //Returns true with d probability
    private boolean flipCoin(double d) {
        return (m_rng.nextDouble() <= d);
    }
}
