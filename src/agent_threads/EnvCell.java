/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agent_threads;

import java.util.LinkedList;

/**
 *
 * @author Matt
 */
public class EnvCell {

    private LinkedList<Particle> m_pList;
    private int m_numAgents;

    public EnvCell() {
        m_pList = new LinkedList<Particle>();
        m_numAgents = 0;
    }

    public EnvCell(LinkedList<Particle> l) {
        m_pList = new LinkedList<Particle>(l);
    }

    public synchronized void addParticle(Particle p) {
        m_pList.add(p);
        //notify();
    }

    public boolean hasParticles() {
        return !m_pList.isEmpty();
    }

    public synchronized void removeAgent() {
        m_numAgents--;
    }

    public int getNumAgents() {
        return m_numAgents;
    }

    public synchronized void addAgent() {
        m_numAgents++;
    }

    public synchronized Particle removeParticle() {
//        while (m_pList.isEmpty()) {
//            try {
//                wait();
//            } catch (InterruptedException e) {
//                System.out.println("Interrupted - " + e.toString());
//            }
//        }

        if (m_pList.isEmpty()) {
            return null;
        }
        else {
            return m_pList.removeFirst();
        }
    }

    public int getNumParticles() {
        return m_pList.size();
    }

    public LinkedList<Particle> getParticles() {
        return m_pList;
    }
}
